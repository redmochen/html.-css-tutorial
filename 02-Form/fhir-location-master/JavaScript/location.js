var base = "http://hapi.fhir.org/baseR4/";
var resource = "Location/";

var loc = {
    "resourceType": "Location",
    "id": "",
    "text": {
        "status": "generated",
        "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\">Clinic For Christ, Cardiology Department</div>"
    },
    "identifier": [{
        "value": ""
    }],
    "status": "active",
    "mode": "instance",
    "name": "",
    "alias": [],
    "description": "",
    "mode": "instance",
    "type": [{
        "coding": [{
            "system": "http://terminology.hl7.org/CodeSystem/v3-RoleCode",
            "code": "",
            "display": ""
        }]
    }],
    "telecom": [{
        "system": "",
        "value": "",
        "use": ""
    }],
    "address": {
        "use": "",
        "type": "both",
        "line": [
            ""
        ],
        "city": "",
        "district": "",
        "state": "",
        "postalCode": "",
        "country": "Taiwan"
    },
    "physicalType": {
        "coding": [{
            "system": "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "code": "",
            "display": ""
        }]
    },
    "position": {
        "longitude": 35.217018,
        "latitude": 31.771959,
        "altitude": 99
    },
    "managingOrganization": {
        "reference": "Organization/"
    }
}

function updateLocation() {
    loc.text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">" + document.getElementById("description").value + "</div>";
    loc.id = document.getElementById("id").value;
    loc.identifier = document.getElementById("identifier").value;
    loc.status = document.getElementById("status").value;
    loc.mode = "instance"; // hardcode
    // 
    loc.name = document.getElementById("name").value;
    // split with regex
    var alias = document.getElementById("alias").value;
    aliasArray = alias.split(',@');
    aliasArray.forEach(element => {
        loc.alias.push(element);
        // console.log(element);
    });
    loc.description = document.getElementById("description").value;
    // telecom
    var telecomSystem = document.getElementsByName("telecom-system");
    var telecomUse = document.getElementsByName("telecom-use");
    var telecomValue = document.getElementsByName("telecom-value");
    loc.telecom[0].system = telecomSystem[0].value;
    loc.telecom[0].value = telecomValue[0].value;
    loc.telecom[0].use = telecomUse[0].value;
    if (telecomSystem.length > 1) {
        console.log("more than 1");
        var telecom = {
            "system": "",
            "value": "",
            "use": ""
        };
        for (i = 1; i < telecomSystem.length; i++) {
            telecom.system = telecomSystem[i].value;
            telecom.value = telecomValue[i].value;
            telecom.use = telecomUse[i].value;
            loc.telecom.push(telecom);
        }
    }
    // type
    var fields = document.getElementsByName("type-code");
    loc.type[0].coding[0].code = fields[0].options[fields[0].selectedIndex].value;
    loc.type[0].coding[0].display = fields[0].options[fields[0].selectedIndex].text;
    if (fields.length > 1) {
        var type = {
            "coding": [{
                "system": "http://terminology.hl7.org/CodeSystem/v3-RoleCode",
                "code": "",
                "display": ""
            }]
        };
        for (i = 1; i < fields.length; i++) {
            type.coding[0].code = fields[i].options[fields[i].selectedIndex].value;
            type.coding[0].display = fields[i].options[fields[i].selectedIndex].text;
            loc.type.push(type);
        }
    }
    // address
    loc.address.use = document.getElementById("add-use").value;
    loc.address.type = document.getElementById("add-type").value;
    loc.address.line = document.getElementById("add-line").value;
    loc.address.city = document.getElementById("add-city").value;
    loc.address.district = document.getElementById("add-district").value;
    loc.address.state = document.getElementById("add-state").value;
    loc.address.postalCode = document.getElementById("add-postal").value;
    loc.address.country = document.getElementById("add-country").value;
    // physical-type
    fields = document.getElementsByName("physicalType-code");
    loc.physicalType.coding[0].code = fields[0].options[fields[0].selectedIndex].value;
    loc.physicalType.coding[0].display = fields[0].options[fields[0].selectedIndex].text;
    // geography-plot
    loc.position.longitude = document.getElementById("position-longitude").value;
    loc.position.latitude = document.getElementById("position-latitude").value;
    loc.position.altitude = document.getElementById("position-altitude").value;

    loc.managingOrganization.reference = "Organization/" + document.getElementById("organization-reference").value;
    var data = JSON.stringify(loc);
    url = base + resource + loc.id;
    HTTPPutData(url, data);
}